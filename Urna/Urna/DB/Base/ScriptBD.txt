﻿drop database URNA;
CREATE DATABASE URNA; 
USE URNA;

CREATE TABLE tb_eleitor (
    id_eleitor int NOT NULL AUTO_INCREMENT,
    nm_nome varchar (100) NOT NULL,
    ds_rg varchar (15) NOT NULL,
    ds_cpf varchar (15) UNIQUE NOT NULL,
    ds_titulo varchar (20) UNIQUE NOT NULL,
    bl_permissaoadm boolean NOT NULL,
    
    PRIMARY KEY (id_eleitor)
);

CREATE TABLE tb_voto (
    id_voto int NOT NULL AUTO_INCREMENT,
    id_candidato int,
    id_eleitor int NOT NULL,
    PRIMARY KEY (id_voto)
);

CREATE TABLE tb_cargo (
    id_cargo int NOT NULL AUTO_INCREMENT,
    nm_cargo varchar (45) NOT NULL,
    PRIMARY KEY (id_cargo)
);

CREATE TABLE tb_candidato (
    id_candidato int NOT NULL AUTO_INCREMENT,
    ds_num_cand varchar (10) UNIQUE  ,
    nm_nome varchar (10),
    id_partido int  ,
    id_cargo int  ,
    ds_foto longblob  ,
    PRIMARY KEY (id_candidato)
);

CREATE TABLE tb_partido (
    id_partido int NOT NULL AUTO_INCREMENT,
    ds_num_partido varchar (6) NOT NULL,
    nm_partido varchar (45) NOT NULL,
    PRIMARY KEY (id_partido)
);

ALTER TABLE tb_voto ADD CONSTRAINT tb_voto_fk0 FOREIGN KEY (id_candidato) REFERENCES tb_candidato(id_candidato);

ALTER TABLE tb_voto ADD CONSTRAINT tb_voto_fk1 FOREIGN KEY (id_eleitor) REFERENCES tb_eleitor(id_eleitor);

ALTER TABLE tb_candidato ADD CONSTRAINT tb_candidato_fk0 FOREIGN KEY (id_partido) REFERENCES tb_partido(id_partido);

ALTER TABLE tb_candidato ADD CONSTRAINT tb_candidato_fk1 FOREIGN KEY (id_cargo) REFERENCES tb_cargo(id_cargo);


create view vw_consultar_candidato as select
tb_candidato.nm_nome,
tb_candidato.ds_num_cand,
tb_partido.nm_partido,
tb_candidato.ds_foto,
tb_cargo.nm_cargo
from tb_candidato
JOIN tb_partido
ON tb_partido.id_partido = tb_candidato.id_partido
JOIN
tb_cargo
ON tb_cargo.id_cargo = tb_candidato.id_cargo;


create view vw_consultar_votos as select
tb_candidato.nm_nome,
tb_candidato.ds_num_cand,
tb_partido.nm_partido,
tb_cargo.nm_cargo,
count(tb_voto.id_voto) as qtd_votos
from tb_candidato
JOIN tb_partido
ON tb_partido.id_partido = tb_candidato.id_partido
JOIN
tb_cargo
ON tb_cargo.id_cargo = tb_candidato.id_cargo
JOIN tb_voto
ON tb_voto.id_candidato = tb_candidato.id_candidato;


create view vw_consultar_candidato_votacao as
select tb_candidato.nm_nome, tb_candidato.ds_foto, tb_cargo.nm_cargo, tb_partido.nm_partido
from tb_candidato
JOIN tb_cargo
ON tb_cargo.id_cargo = tb_candidato.id_cargo
Join tb_partido
ON tb_partido.id_partido = tb_candidato.id_partido ;


INSERT INTO tb_eleitor(nm_nome,ds_rg,ds_cpf,ds_titulo,bl_permissaoadm) VALUES ("Airton", "3443256346", "11111111111", "222222222222",true);
INSERT INTO tb_eleitor(nm_nome,ds_rg,ds_cpf,ds_titulo,bl_permissaoadm) VALUES ("Aldair", "12435686346", "33333333333", "444444444444",false);
INSERT INTO tb_eleitor(nm_nome,ds_rg,ds_cpf,ds_titulo,bl_permissaoadm) VALUES ("Lucas", "4365421", "4564375", "1235321",false);
INSERT INTO tb_eleitor(nm_nome,ds_rg,ds_cpf,ds_titulo,bl_permissaoadm) VALUES ("Fortren", "1235474", "735731456", "34678458",false);
INSERT INTO tb_eleitor(nm_nome,ds_rg,ds_cpf,ds_titulo,bl_permissaoadm) VALUES ("Astolfo", "3467865837", "3345644124", "56376543",false);







INSERT INTO tb_partido(nm_partido, ds_num_partido) VALUES ("PT", "11");
INSERT INTO tb_partido(nm_partido, ds_num_partido) VALUES ("PSDB", "12");
INSERT INTO tb_partido(nm_partido, ds_num_partido) VALUES ("PSOL", "13");






INSERT INTO tb_cargo(nm_cargo) VALUES ("Presidente");
INSERT INTO tb_cargo(nm_cargo) VALUES ("Governador");
INSERT INTO tb_cargo(nm_cargo) VALUES ("Senador");
INSERT INTO tb_cargo(nm_cargo) VALUES ("Deputado Federal");
INSERT INTO tb_cargo(nm_cargo) VALUES ("Deputado Estadual");








INSERT INTO tb_candidato(id_candidato,ds_num_cand,nm_nome,id_partido,id_cargo,ds_foto) VALUES ('1',null,null,null,null,null);

select * from tb_eleitor;