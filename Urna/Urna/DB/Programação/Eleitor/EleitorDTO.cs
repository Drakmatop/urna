﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Eleitor
{
  public  class EleitorDTO
    {
        public int id_eleitor { get; set; }
        public string nm_nome { get; set; }
        public string ds_rg { get; set; }
        public string ds_cpf { get; set; }
        public string ds_titulo { get; set; }
        public Boolean bl_permissaoadm { get; set; }
    }
}
