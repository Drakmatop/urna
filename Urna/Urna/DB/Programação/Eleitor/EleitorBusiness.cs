﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Eleitor
{
  public  class EleitorBusiness
    {
        public int Salvar(EleitorDTO dto)
        {
            if (dto.nm_nome == string.Empty)
                throw new ArgumentException("O nome é obrigatório");

            if (dto.ds_titulo == string.Empty)
                throw new ArgumentException("O título de eleitor é obrigatório");

            if (dto.ds_cpf == string.Empty)
                throw new ArgumentException("CPF é obrigatório");

            if (dto.ds_rg == string.Empty)
                throw new ArgumentException("O RG é obrigatório");


            EleitorDatabase database = new EleitorDatabase();
            return database.Salvar(dto);
        }

        public List<EleitorDTO> Listar()
        {
            EleitorDatabase database = new EleitorDatabase();
            return database.Listar();
        }

        public List<EleitorDTO> Consultar(string eleitor)
        {
            EleitorDatabase database = new EleitorDatabase();
            return database.Consultar(eleitor);

        }

        public EleitorDTO Logar(string Cpf, string Titulo)
        {
            if (Cpf == string.Empty)
            {
                throw new ArgumentException("Cpf é obrigatório");
            }
            if (Titulo == string.Empty)
            {
                throw new ArgumentException("Titulo é obrigatória");
            }
            EleitorDatabase db = new EleitorDatabase();

            return db.Logar(Cpf, Titulo);

        }
    }
}
