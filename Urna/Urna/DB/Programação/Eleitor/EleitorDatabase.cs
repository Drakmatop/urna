﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Programação.Eleitor
{
  public  class EleitorDatabase
    {
        public int Salvar(EleitorDTO eleitor)
        {
            string script =
                @"INSERT INTO tb_eleitor
                  (
                    id_eleitor,
                    nm_nome,
                    ds_rg,
                    ds_cpf,
                    ds_titulo,
                    bl_permissaoadm
                  )
                  VALUES
                  (
                    @id_eleitor,
                    @nm_nome,
                    @ds_rg,
                    @ds_cpf,
                    @ds_titulo,
                    @bl_permissaoadm
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_eleitor", eleitor.id_eleitor));
            parms.Add(new MySqlParameter("nm_nome", eleitor.nm_nome));
            parms.Add(new MySqlParameter("ds_rg", eleitor.ds_rg));
            parms.Add(new MySqlParameter("ds_cpf", eleitor.ds_cpf));
            parms.Add(new MySqlParameter("ds_titulo", eleitor.ds_titulo));
            parms.Add(new MySqlParameter("bl_permissaoadm", eleitor.bl_permissaoadm));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public List<EleitorDTO> Consultar(string eleitor)
        {
            string script =
                @"SELECT * FROM tb_eleitor
                  WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + eleitor + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EleitorDTO> eleitores = new List<EleitorDTO>();
            while (reader.Read())
            {
                EleitorDTO novoeleitor = new EleitorDTO();
                novoeleitor.nm_nome = reader.GetString("nm_nome");
                novoeleitor.ds_cpf = reader.GetString("ds_cpf");
                novoeleitor.ds_rg = reader.GetString("ds_rg");
                novoeleitor.ds_titulo = reader.GetString("ds_titulo");
                novoeleitor.bl_permissaoadm = reader.GetBoolean("bl_permissaoadm");
               
                eleitores.Add(novoeleitor);
            }
            return eleitores;
        }
        public List<EleitorDTO> Listar()
        {
            string script =
                 @"SELECT * FROM tb_eleitor";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EleitorDTO> eleitores = new List<EleitorDTO>();
            while (reader.Read())
            {
                EleitorDTO novoeleitor = new EleitorDTO();
                novoeleitor.id_eleitor = reader.GetInt32("id_eleitor");
                novoeleitor.nm_nome = reader.GetString("nm_nome");
                novoeleitor.ds_cpf = reader.GetString("ds_cpf");
                novoeleitor.ds_rg = reader.GetString("ds_rg");
                novoeleitor.ds_titulo = reader.GetString("ds_titulo");
                novoeleitor.bl_permissaoadm = reader.GetBoolean("bl_permissaoadm");

                eleitores.Add(novoeleitor);
            }
            return eleitores;

        }

        public EleitorDTO Logar(string Cpf, string Titulo)
        {
            string script = @"Select * FROM tb_eleitor WHERE ds_cpf = @ds_cpf
                        AND ds_titulo = @ds_titulo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", Cpf));
            parms.Add(new MySqlParameter("ds_titulo", Titulo));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            EleitorDTO Eleitor = null;
            if (reader.Read())
            {
                Eleitor = new EleitorDTO();
                Eleitor.id_eleitor = reader.GetInt32("id_eleitor");
                Eleitor.ds_cpf = reader.GetString("ds_cpf");
                Eleitor.nm_nome = reader.GetString("nm_nome");
                Eleitor.ds_rg = reader.GetString("ds_rg");
                Eleitor.ds_titulo = reader.GetString("ds_titulo");
                Eleitor.bl_permissaoadm = reader.GetBoolean("bl_permissaoadm");
            }
            reader.Close();

            return Eleitor;
        }











    }
}
