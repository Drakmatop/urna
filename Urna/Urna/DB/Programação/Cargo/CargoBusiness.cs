﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Cargo
{
   public class CargoBusiness
    {
        public int Salvar(CargoDTO dto)
        {
            if (dto.nm_cargo == string.Empty)
                throw new ArgumentException(" O cargo é obrigatório");

            CargoDatabase database = new CargoDatabase();
            return database.Salvar(dto);
        }

        public List<CargoDTO> Listar()
        {
            CargoDatabase database = new CargoDatabase();
            return database.Listar();
        }

        public List<CargoDTO> Consultar(string cargo)
        {
            CargoDatabase database = new CargoDatabase();
            return database.Consultar(cargo);
        }

    }
}
