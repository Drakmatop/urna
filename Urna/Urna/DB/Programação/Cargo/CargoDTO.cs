﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Cargo
{
   public class CargoDTO
    {
        public int id_cargo { get; set; }
        public string nm_cargo { get; set; }
    }
}
