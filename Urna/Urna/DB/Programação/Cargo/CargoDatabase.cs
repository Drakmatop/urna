﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Programação.Cargo
{
    public class CargoDatabase
    {
        public int Salvar(CargoDTO cargo)
        {
            string script =
                @"INSERT INTO tb_cargo
                  (
                    id_cargo,
                    nm_cargo
                  )
                  VALUES
                  (
                    @id_cargo,
                    @nm_cargo
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cargo", cargo.id_cargo));
            parms.Add(new MySqlParameter("nm_cargo", cargo.nm_cargo));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public List<CargoDTO> Consultar(string nome)
        {
            string script = @"select * from tb_cargo
            WHERE nm_cargo = @nm_cargo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cargo", nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CargoDTO> lista = new List<CargoDTO>();

            while (reader.Read())
            {
                CargoDTO dto = new CargoDTO();
                dto.id_cargo = reader.GetInt32("id_cargo");
                dto.nm_cargo = reader.GetString("nm_cargo");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<CargoDTO> Listar()
        {
            string script = @"select * from tb_cargo
            ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CargoDTO> lista = new List<CargoDTO>();

            while (reader.Read())
            {
                CargoDTO dto = new CargoDTO();
                dto.id_cargo = reader.GetInt32("id_cargo");
                dto.nm_cargo = reader.GetString("nm_cargo");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

