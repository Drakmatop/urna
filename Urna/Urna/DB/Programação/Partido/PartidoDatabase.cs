﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Programação.Partido
{
 public class PartidoDatabase
    {
        public int Salvar(PartidoDTO partido)
        {
            string script =
                @"INSERT INTO tb_partido
                  (
                    id_partido,
                    ds_num_partido,
                    nm_partido              
                  )
                  VALUES
                  (
                    @id_partido,
                    @ds_num_partido,
                    @nm_partido                    
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_num_partido", partido.ds_num_partido));
            parms.Add(new MySqlParameter("nm_partido", partido.nm_partido));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public List<PartidoDTO> Consultar(string partido)
        {
            string script = @"select * from tb_partido
            WHERE nm_partido = @nm_partido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_partido", partido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PartidoDTO> lista = new List<PartidoDTO>();

            while (reader.Read())
            {
                PartidoDTO dto = new PartidoDTO();
                dto.id_partido = reader.GetInt32("id_partido");
                dto.ds_num_partido = reader.GetString("ds_num_partido");
                dto.nm_partido = reader.GetString("nm_partido");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<PartidoDTO> Listar()
        {
            string script = @"select * from tb_partido
            ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PartidoDTO> lista = new List<PartidoDTO>();

            while (reader.Read())
            {
                PartidoDTO dto = new PartidoDTO();
                dto.id_partido = reader.GetInt32("id_partido");
                dto.ds_num_partido = reader.GetString("ds_num_partido");
                dto.nm_partido = reader.GetString("nm_partido");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
