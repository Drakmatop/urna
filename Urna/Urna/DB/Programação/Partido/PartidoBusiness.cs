﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Partido
{
  public  class PartidoBusiness
    {
        public int Salvar(PartidoDTO dto)
        {
            if (dto.nm_partido == string.Empty)
                throw new ArgumentException("Nome é obrigatório");

            if (dto.ds_num_partido == string.Empty)
                throw new ArgumentException("O número do partido é obrigatório");



            PartidoDatabase database = new PartidoDatabase();
            return database.Salvar(dto);
        }

        public List<PartidoDTO> Listar()
        {
            PartidoDatabase database = new PartidoDatabase();
            return database.Listar();
        }

        public List<PartidoDTO> Consultar(string partido)
        {
            PartidoDatabase database = new PartidoDatabase();
            return database.Consultar(partido);

        }

    }
}
