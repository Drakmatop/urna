﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Partido
{
   public class PartidoDTO
    {
        public int id_partido { get; set; }
        public string ds_num_partido { get; set; }
        public string nm_partido { get; set; }
    }
}
