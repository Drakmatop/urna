﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Voto
{
public class VotoDTO
    {
        public int id_voto { get; set; }
        public int id_candidato { get; set; }
        public int id_eleitor { get; set; }
    }
}
