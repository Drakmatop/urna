﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Voto
{
    public class VwConsultarVoto
    {
        public string Nome { get; set; }

        public string Partido { get; set; }

        public string Cargo { get; set; }

        public string NCandidato { get; set; }

        public int QtdVotos { get; set; }



    }
}
