﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Programação.Voto
{
   public class VotoDatabase
    {
        public int Salvar(VotoDTO voto)
        {
            string script =
                @"INSERT INTO tb_voto
                  (
                    id_voto,
                    id_candidato,
                    id_eleitor                    
                  )
                  VALUES
                  (
                    @id_voto,
                    @id_candidato,
                    @id_eleitor                    
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_voto", voto.id_voto));
            parms.Add(new MySqlParameter("id_candidato", voto.id_candidato));
            parms.Add(new MySqlParameter("id_eleitor", voto.id_eleitor));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public List<VwConsultarVoto> Consultar(string Nome)
        {
            string script = @"select * from vw_consultar_candidato
                WHERE nm_nome = @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarVoto> lista = new List<VwConsultarVoto>();

            while (reader.Read())
            {
                VwConsultarVoto dto = new VwConsultarVoto();
                dto.Nome = reader.GetString("nm_nome");
                dto.NCandidato = reader.GetString("ds_num_cand");
                dto.Partido = reader.GetString("nm_partido");
                dto.Cargo = reader.GetString("nm_cargo");
                dto.QtdVotos = reader.GetInt32("qtd_votos");


                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }


    }
}
