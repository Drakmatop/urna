﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Voto
{
  public  class VotoBusiness
    {

        public int Salvar(VotoDTO dto)
        {
            VotoDatabase database = new VotoDatabase();
            return database.Salvar(dto);
        }

        public List<VwConsultarVoto> Consultar(string Nome)
        {
            if (Nome == string.Empty)
            {
                Nome = "%"; 
            }

            VotoDatabase db = new VotoDatabase();
            return db.Consultar(Nome);

        }


    }
}
