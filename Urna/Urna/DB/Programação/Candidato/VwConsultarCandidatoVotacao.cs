﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Candidato
{
    public class VwConsultarCandidatoVotacao
    {
        public string Nome { get; set; }

        public string Cargo { get; set; }

        public string Partido { get; set; }

        public string Foto { get; set; }

        public int IdCandidato { get; set; }
    }
}
