﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Candidato
{
    public class CandidatoDTO
    {
        public int id_candidato { get; set; }
        public string ds_num_cand { get; set; }
        public string nm_nome { get; set; }
        public int id_partido { get; set; }
        public int id_cargo { get; set; }
        public string ds_foto { get; set; }
    }
}
