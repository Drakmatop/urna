﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Programação.Candidato
{
    public class CandidatoDatabase
    {
        public int Salvar(CandidatoDTO candidato)
        {
            string script =
                @"INSERT INTO tb_candidato
                  (
                    id_candidato,
                    ds_num_cand,
                    nm_nome,
                    id_partido,
                    id_cargo,
                    ds_foto
                  )
                  VALUES
                  (
                    @id_candidato,
                    @ds_num_cand,
                    @nm_nome,
                    @id_partido,
                    @id_cargo,
                    @ds_foto
                  )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_candidato", candidato.id_candidato));
            parms.Add(new MySqlParameter("ds_num_cand", candidato.ds_num_cand));
            parms.Add(new MySqlParameter("nm_nome", candidato.nm_nome));
         
            parms.Add(new MySqlParameter("id_partido", candidato.id_partido));
            parms.Add(new MySqlParameter("id_cargo", candidato.id_cargo));
            parms.Add(new MySqlParameter("ds_foto", candidato.ds_foto));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<VwConsultarCandidato> Consultar(string Nome)
        {
            string script = @"select * from vw_consultar_candidato
                WHERE nm_nome = @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarCandidato> lista = new List<VwConsultarCandidato>();

            while (reader.Read())
            {
                VwConsultarCandidato dto = new VwConsultarCandidato();
                dto.Nome = reader.GetString("nm_nome");
                dto.NCandidato = reader.GetString("ds_num_cand");
                dto.Partido = reader.GetString("nm_partido");
                dto.Cargo = reader.GetString("nm_cargo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;



        }
        public List<VwConsultarCandidato> Listar()
        {
            string script = @"select * from vw_consultar_candidato
               ";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarCandidato> lista = new List<VwConsultarCandidato>();

            while (reader.Read())
            {
                VwConsultarCandidato dto = new VwConsultarCandidato();
                dto.Nome = reader.GetString("nm_nome");
                dto.NCandidato = reader.GetString("ds_num_cand");
                dto.Partido = reader.GetString("nm_partido");
                dto.Cargo = reader.GetString("nm_cargo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
        public List<VwConsultarCandidatoVotacao> ConsultarPorNumero(string NumeroCandidato)
        {
            string script = @"select tb_candidato.id_candidato,tb_candidato.nm_nome, tb_candidato.ds_foto, tb_cargo.nm_cargo, tb_partido.nm_partido
                              from tb_candidato
                              JOIN tb_cargo
                              ON tb_cargo.id_cargo = tb_candidato.id_cargo
                              Join tb_partido
                              ON tb_partido.id_partido = tb_candidato.id_partido
                              Where ds_num_cand = @ds_num_cand ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_num_cand", NumeroCandidato));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarCandidatoVotacao> dto = new List<VwConsultarCandidatoVotacao>();
            while (reader.Read())
            {
                VwConsultarCandidatoVotacao newdto = new VwConsultarCandidatoVotacao();
                newdto.Nome = reader.GetString("nm_nome");
                newdto.Partido = reader.GetString("nm_partido");
                newdto.Cargo = reader.GetString("nm_cargo");
                newdto.Foto = reader.GetString("ds_foto");
                newdto.IdCandidato = reader.GetInt32("id_candidato");

                dto.Add(newdto);

            }
            reader.Close();

            return dto;

        }






        }
}
