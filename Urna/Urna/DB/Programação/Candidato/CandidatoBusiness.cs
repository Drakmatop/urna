﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação.Candidato
{
   public class CandidatoBusiness
    {
        public int Salvar(CandidatoDTO dto)
        {
            if (dto.nm_nome == string.Empty)
                throw new ArgumentException("Nome é obrigatório.");

            if (dto.ds_num_cand == string.Empty)
                throw new ArgumentException("O número do candidato é obrigatório.");

            if(dto.id_partido == 0)
            {
                throw new ArgumentException("Partido obrigatório.");
            }
            if (dto.id_cargo == 0)
            {
                throw new ArgumentException("Cargo obrigatório.");
            }
            if(dto.ds_foto == string.Empty)
            {
                throw new ArgumentException("Foto obrigatório.");
            }





            CandidatoDatabase database = new CandidatoDatabase();
            return database.Salvar(dto);
        }


        public List<VwConsultarCandidato>Consultar(string candidato)
        {
            CandidatoDatabase database = new CandidatoDatabase();
            return database.Consultar(candidato);

        }
        public List<VwConsultarCandidato> Listar()
        {
            CandidatoDatabase database = new CandidatoDatabase();
            return database.Listar();

        }

        public List<VwConsultarCandidatoVotacao> ConsultarPorNumero(string NumeroCandidato)
        {
            CandidatoDatabase db = new CandidatoDatabase();
            return db.ConsultarPorNumero(NumeroCandidato);
        }

    }
}
