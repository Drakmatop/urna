﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Programação
{
   public class ImagemPlugin
    {
        public static string ConverterParaString(Image imagem)
        {
            MemoryStream memoria = new MemoryStream();

            imagem.Save(memoria, imagem.RawFormat);
            byte[] imagebytes = memoria.ToArray();

            string ImagemEmTexto = Convert.ToBase64String(imagebytes);
            return ImagemEmTexto;
        }
        public static Image ConverterParaImagem(string imagemEmTexto)
        {
            byte[] bytes = Convert.FromBase64String(imagemEmTexto);
            Image imagem = Image.FromStream(new MemoryStream(bytes));
            return imagem;
        }

    }
}
