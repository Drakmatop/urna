﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação.Candidato;

namespace Urna.Telas.Bscar
{
    public partial class ConsultarCandiatos : Form
    {
        public ConsultarCandiatos()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            CandidatoBusiness business = new CandidatoBusiness();
            List<VwConsultarCandidato> dto = business.Listar();

            cboCandidato.ValueMember = nameof(VwConsultarCandidato.Nome);
            cboCandidato.DisplayMember = nameof(VwConsultarCandidato.Nome);
            cboCandidato.DataSource = dto;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            VwConsultarCandidato candidato = cboCandidato.SelectedItem as VwConsultarCandidato;

            CandidatoBusiness business = new CandidatoBusiness();

            List<VwConsultarCandidato> lista = business.Consultar(candidato.Nome);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void ConsultarCandiatos_Load(object sender, EventArgs e)
        {

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
