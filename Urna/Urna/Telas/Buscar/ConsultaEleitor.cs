﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação.Eleitor;

namespace Urna.Telas.Bscar
{
    public partial class ConsultaEleitor : Form
    {
        public ConsultaEleitor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            EleitorBusiness business = new EleitorBusiness();
            List<EleitorDTO> dto = business.Listar();

            comboBox1.ValueMember = nameof(EleitorDTO.id_eleitor);
            comboBox1.DisplayMember = nameof(EleitorDTO.nm_nome);
            comboBox1.DataSource = dto;

        }


        private void button1_Click(object sender, EventArgs e)
        {
            EleitorDTO dto = comboBox1.SelectedItem as EleitorDTO;

            EleitorBusiness business = new EleitorBusiness();
            List<EleitorDTO> lista = business.Consultar(dto.nm_nome);

            dgvEleitor.AutoGenerateColumns = false;
            dgvEleitor.DataSource = lista;


        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
