﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação.Partido;

namespace Urna.Telas.Bscar
{
    public partial class ConsularPartido : Form
    {
        public ConsularPartido()
        {
            InitializeComponent();
            CarregarCombo();

            button1_Click(null, null);
        }
        void CarregarCombo()
        {
            PartidoBusiness business = new PartidoBusiness();
            List<PartidoDTO> dto = business.Listar();

            comboBox1.ValueMember = nameof(PartidoDTO.id_partido);
            comboBox1.DisplayMember = nameof(PartidoDTO.nm_partido);
            comboBox1.DataSource = dto;


        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PartidoDTO dto = comboBox1.SelectedItem as PartidoDTO;

            PartidoBusiness business = new PartidoBusiness();

            List<PartidoDTO> lista = business.Consultar(dto.nm_partido);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
