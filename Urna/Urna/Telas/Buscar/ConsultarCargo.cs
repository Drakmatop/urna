﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação.Cargo;

namespace Urna.Telas.Bscar
{
    public partial class ConsultarCargo : Form
    {
        public ConsultarCargo()
        {
            InitializeComponent();
            button1_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargoBusiness business = new CargoBusiness();
            List<CargoDTO> dto = business.Listar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = dto;

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
