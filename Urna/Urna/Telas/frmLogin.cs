﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação.Eleitor;

namespace Urna.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();

        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.bl_permissaoadm == true)
            {
                frmMenu tela = new frmMenu();
                tela.Show();
                this.Hide();


            }

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                string cpf = txtCpf.Text;
                cpf = cpf.Replace(".", "");
                cpf = cpf.Replace("-", "");


                string titulo = txtTitulo.Text;
                titulo = titulo.Replace(".", "");
                titulo = titulo.Replace("-", "");




                EleitorBusiness business = new EleitorBusiness();
                EleitorDTO Eleitor = business.Logar(cpf, titulo);

                if (Eleitor != null)
                {
                    UserSession.UsuarioLogado = Eleitor;

                    if (UserSession.UsuarioLogado.bl_permissaoadm == true)
                    {
                        frmMenu tela = new frmMenu();
                        tela.Show();
                        this.Hide();

                    }
                    else
                    {
                       
                        VotacaoPresidente frm = new VotacaoPresidente();
                        frm.Show();
                        Hide();
                    }
                }

                else
                {
                    MessageBox.Show("Credenciais inválidas.");
                }

                
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro " + ex.Message);
            }
        }
    }
}

