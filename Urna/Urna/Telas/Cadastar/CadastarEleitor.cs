﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação;
using Urna.DB.Programação.Eleitor;

namespace Urna.Telas.Cadastar
{
    public partial class CadastarEleitor : Form
    {
        Validação v = new Validação();
        public CadastarEleitor()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.letras(e);
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.numeros(e);
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                EleitorDTO dto = new EleitorDTO();
                dto.nm_nome = txtNome.Text;
                dto.ds_cpf = txtCpf.Text;
                dto.ds_rg = txtRG.Text;
                dto.ds_titulo = txtTitulo.Text;

                EleitorBusiness business = new EleitorBusiness();
                business.Salvar(dto);
                MessageBox.Show("Salvo com sucesso.");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
            

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
