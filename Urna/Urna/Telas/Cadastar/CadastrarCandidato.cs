﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação;
using Urna.DB.Programação.Candidato;
using Urna.DB.Programação.Cargo;
using Urna.DB.Programação.Partido;

namespace Urna.Telas.Cadastar
{
    public partial class CadastrarCandidato : Form
    {
        Validação V = new Validação();
        public CadastrarCandidato()
        {
            InitializeComponent();
            CarregarCombo();
        }

        void CarregarCombo()
        {
            CargoBusiness business = new CargoBusiness();
            List<CargoDTO> cargolista = business.Listar();
            cmbCargo.ValueMember = nameof(CargoDTO.id_cargo);
            cmbCargo.DisplayMember = nameof(CargoDTO.nm_cargo);
            cmbCargo.DataSource = cargolista;

            PartidoBusiness businesss = new PartidoBusiness();
            List<PartidoDTO> lista = businesss.Listar();
            cmbPartido.ValueMember = nameof(PartidoDTO.id_partido);
            cmbPartido.DisplayMember = nameof(PartidoDTO.nm_partido);
            cmbPartido.DataSource = lista;

            

        }







        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.letras(e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            V.numeros(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CargoDTO cargoid = cmbCargo.SelectedItem as CargoDTO;
                PartidoDTO partidodto = cmbPartido.SelectedItem as PartidoDTO;

                CandidatoDTO dto = new CandidatoDTO();
                dto.nm_nome = txtNome.Text;
                dto.ds_num_cand = txtNCandidato.Text;
                dto.id_cargo = cargoid.id_cargo;
                dto.id_partido = partidodto.id_partido;

                dto.ds_foto = ImagemPlugin.ConverterParaString(imgcandidato.Image);

                CandidatoBusiness business = new CandidatoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Salvo com sucesso.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }


        }

        private void cmbCargo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbPartido_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void imgcandidato_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgcandidato.ImageLocation = dialog.FileName;
            }
        }

        private void txtNCandidato_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
