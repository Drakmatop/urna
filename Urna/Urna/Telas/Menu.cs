﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.Telas.Bscar;
using Urna.Telas.Cadastar;

namespace Urna.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btncadastrareleitor_Click(object sender, EventArgs e)
        {
            CadastarEleitor tela = new CadastarEleitor();
            tela.Show();
            this.Hide();
        }

        private void btncadastrarcadidato_Click(object sender, EventArgs e)
        {
            CadastrarCandidato tela = new CadastrarCandidato();
            tela.Show();
            this.Hide();
        }

        private void btnconsultacargo_Click(object sender, EventArgs e)
        {
            ConsultarCargo tela = new ConsultarCargo();
            tela.Show();
            this.Hide();
        }

        private void btnConsultarEleitor_Click(object sender, EventArgs e)
        {
            ConsultaEleitor tela = new ConsultaEleitor();
            tela.Show();
            this.Hide();
        }

        private void btnconsultarcandidato_Click(object sender, EventArgs e)
        {
            ConsultarCandiatos tela = new ConsultarCandiatos();
            tela.Show();
            this.Hide();
        }

        private void btnconsultarpardido_Click(object sender, EventArgs e)
        {
            ConsularPartido tela = new ConsularPartido();
            tela.Show();
            this.Hide();
        }

        private void btnvotar_Click(object sender, EventArgs e)
        {
            VotacaoPresidente tela = new VotacaoPresidente();
            tela.Show();
            this.Hide();
        }
    }
}
