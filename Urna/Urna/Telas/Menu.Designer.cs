﻿namespace Urna.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncadastrareleitor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnconsultacargo = new System.Windows.Forms.Button();
            this.btncadastrarcadidato = new System.Windows.Forms.Button();
            this.btnconsultarcandidato = new System.Windows.Forms.Button();
            this.btnvotar = new System.Windows.Forms.Button();
            this.btnConsultarEleitor = new System.Windows.Forms.Button();
            this.btnconsultarpardido = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btncadastrareleitor
            // 
            this.btncadastrareleitor.BackColor = System.Drawing.Color.Transparent;
            this.btncadastrareleitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncadastrareleitor.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btncadastrareleitor.Location = new System.Drawing.Point(31, 84);
            this.btncadastrareleitor.Name = "btncadastrareleitor";
            this.btncadastrareleitor.Size = new System.Drawing.Size(87, 62);
            this.btncadastrareleitor.TabIndex = 0;
            this.btncadastrareleitor.Text = "Cadastrar Eleitor";
            this.btncadastrareleitor.UseVisualStyleBackColor = false;
            this.btncadastrareleitor.Click += new System.EventHandler(this.btncadastrareleitor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(93, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 72);
            this.label1.TabIndex = 1;
            this.label1.Text = "URNA";
            // 
            // btnconsultacargo
            // 
            this.btnconsultacargo.BackColor = System.Drawing.Color.Transparent;
            this.btnconsultacargo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconsultacargo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnconsultacargo.Location = new System.Drawing.Point(258, 84);
            this.btnconsultacargo.Name = "btnconsultacargo";
            this.btnconsultacargo.Size = new System.Drawing.Size(87, 64);
            this.btnconsultacargo.TabIndex = 2;
            this.btnconsultacargo.Text = "Consultar Cargo";
            this.btnconsultacargo.UseVisualStyleBackColor = false;
            this.btnconsultacargo.Click += new System.EventHandler(this.btnconsultacargo_Click);
            // 
            // btncadastrarcadidato
            // 
            this.btncadastrarcadidato.BackColor = System.Drawing.Color.Transparent;
            this.btncadastrarcadidato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncadastrarcadidato.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btncadastrarcadidato.Location = new System.Drawing.Point(147, 84);
            this.btncadastrarcadidato.Name = "btncadastrarcadidato";
            this.btncadastrarcadidato.Size = new System.Drawing.Size(87, 62);
            this.btncadastrarcadidato.TabIndex = 4;
            this.btncadastrarcadidato.Text = "Cadastrar Candidato";
            this.btncadastrarcadidato.UseVisualStyleBackColor = false;
            this.btncadastrarcadidato.Click += new System.EventHandler(this.btncadastrarcadidato_Click);
            // 
            // btnconsultarcandidato
            // 
            this.btnconsultarcandidato.BackColor = System.Drawing.Color.Transparent;
            this.btnconsultarcandidato.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconsultarcandidato.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnconsultarcandidato.Location = new System.Drawing.Point(147, 152);
            this.btnconsultarcandidato.Name = "btnconsultarcandidato";
            this.btnconsultarcandidato.Size = new System.Drawing.Size(87, 64);
            this.btnconsultarcandidato.TabIndex = 5;
            this.btnconsultarcandidato.Text = "Consultar Candidato";
            this.btnconsultarcandidato.UseVisualStyleBackColor = false;
            this.btnconsultarcandidato.Click += new System.EventHandler(this.btnconsultarcandidato_Click);
            // 
            // btnvotar
            // 
            this.btnvotar.BackColor = System.Drawing.Color.Transparent;
            this.btnvotar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnvotar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnvotar.Location = new System.Drawing.Point(31, 222);
            this.btnvotar.Name = "btnvotar";
            this.btnvotar.Size = new System.Drawing.Size(314, 63);
            this.btnvotar.TabIndex = 6;
            this.btnvotar.Text = "VOTAR";
            this.btnvotar.UseVisualStyleBackColor = false;
            this.btnvotar.Click += new System.EventHandler(this.btnvotar_Click);
            // 
            // btnConsultarEleitor
            // 
            this.btnConsultarEleitor.BackColor = System.Drawing.Color.Transparent;
            this.btnConsultarEleitor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarEleitor.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnConsultarEleitor.Location = new System.Drawing.Point(31, 154);
            this.btnConsultarEleitor.Name = "btnConsultarEleitor";
            this.btnConsultarEleitor.Size = new System.Drawing.Size(87, 62);
            this.btnConsultarEleitor.TabIndex = 7;
            this.btnConsultarEleitor.Text = "Consultar Eleitor";
            this.btnConsultarEleitor.UseVisualStyleBackColor = false;
            this.btnConsultarEleitor.Click += new System.EventHandler(this.btnConsultarEleitor_Click);
            // 
            // btnconsultarpardido
            // 
            this.btnconsultarpardido.BackColor = System.Drawing.Color.Transparent;
            this.btnconsultarpardido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconsultarpardido.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnconsultarpardido.Location = new System.Drawing.Point(258, 152);
            this.btnconsultarpardido.Name = "btnconsultarpardido";
            this.btnconsultarpardido.Size = new System.Drawing.Size(87, 64);
            this.btnconsultarpardido.TabIndex = 8;
            this.btnconsultarpardido.Text = "Consultar Pardido";
            this.btnconsultarpardido.UseVisualStyleBackColor = false;
            this.btnconsultarpardido.Click += new System.EventHandler(this.btnconsultarpardido_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Urna.Properties.Resources._4a2bb6238c6b11d06669423831805e78;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(374, 297);
            this.Controls.Add(this.btnconsultarpardido);
            this.Controls.Add(this.btnConsultarEleitor);
            this.Controls.Add(this.btnvotar);
            this.Controls.Add(this.btnconsultarcandidato);
            this.Controls.Add(this.btncadastrarcadidato);
            this.Controls.Add(this.btnconsultacargo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btncadastrareleitor);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncadastrareleitor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnconsultacargo;
        private System.Windows.Forms.Button btncadastrarcadidato;
        private System.Windows.Forms.Button btnconsultarcandidato;
        private System.Windows.Forms.Button btnvotar;
        private System.Windows.Forms.Button btnConsultarEleitor;
        private System.Windows.Forms.Button btnconsultarpardido;
    }
}