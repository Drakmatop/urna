﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação;
using Urna.DB.Programação.Candidato;
using Urna.DB.Programação.Eleitor;
using Urna.DB.Programação.Voto;

namespace Urna.Telas
{
    public partial class VotaçãoDepestadual : Form
    {
        public VotaçãoDepestadual()
        {
            InitializeComponent();
        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CandidatoBusiness business = new CandidatoBusiness();
                List<VwConsultarCandidatoVotacao> candidato = business.ConsultarPorNumero(txtnumero.Text);

                dgvcandidato.AutoGenerateColumns = false;
                dgvcandidato.DataSource = candidato;

                VwConsultarCandidatoVotacao dto = dgvcandidato.CurrentRow.DataBoundItem as VwConsultarCandidatoVotacao;


                imgcandidato.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                VotoDTO dto = new VotoDTO();
                dto.id_candidato = 1;
                dto.id_eleitor = UserSession.UsuarioLogado.id_eleitor;

                VotoBusiness business = new VotoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Voto do Deputado Estadual feito sucesso. Obrigado por colaborar a escolher o caminho do Brasil.");
                Application.Exit();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

            try
            {
                VotoBusiness business = new VotoBusiness();
                VwConsultarCandidatoVotacao dto = dgvcandidato.CurrentRow.DataBoundItem as VwConsultarCandidatoVotacao;

                VotoDTO voto = new VotoDTO();
                voto.id_candidato = dto.IdCandidato;
                voto.id_eleitor = UserSession.UsuarioLogado.id_eleitor;
                business.Salvar(voto);
                MessageBox.Show("Voto do Deputado Estadual feito sucesso. Obrigado por colaborar a escolher o caminho do Brasil.");

                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void btnvoltar_Click_1(object sender, EventArgs e)
        {

        }

        private void dgvcandidato_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtnumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void imgcandidato_Click(object sender, EventArgs e)
        {

        }
    }
}
