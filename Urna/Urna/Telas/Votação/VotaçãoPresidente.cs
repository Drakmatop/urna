﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Programação;
using Urna.DB.Programação.Candidato;
using Urna.DB.Programação.Eleitor;
using Urna.DB.Programação.Voto;

namespace Urna.Telas
{
    public partial class VotacaoPresidente : Form
    {
        public VotacaoPresidente()
        {
            InitializeComponent();
            VerificarPermissoes();
        }
        void VerificarPermissoes()
        {
            if(UserSession.UsuarioLogado.bl_permissaoadm == false)
            {
                btnvoltar.Enabled = false;
            }
        }




        private void button12_Click(object sender, EventArgs e)
        {
            txtnumero.Clear();
            dgvcandidato.ClearSelection();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtnumero.Text = txtnumero.Text + "0";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                VotoBusiness business = new VotoBusiness();
                VwConsultarCandidatoVotacao dto = dgvcandidato.CurrentRow.DataBoundItem as VwConsultarCandidatoVotacao;

                VotoDTO voto = new VotoDTO();
                voto.id_candidato = dto.IdCandidato;
                voto.id_eleitor = UserSession.UsuarioLogado.id_eleitor;
                business.Salvar(voto);
                MessageBox.Show("Voto do presidente feito sucesso, agora vote no governador");
                VotaçãoGovernador tela = new VotaçãoGovernador();
                tela.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CandidatoBusiness business = new CandidatoBusiness();
                List<VwConsultarCandidatoVotacao> candidato = business.ConsultarPorNumero(txtnumero.Text);

                dgvcandidato.AutoGenerateColumns = false;
                dgvcandidato.DataSource = candidato;

                VwConsultarCandidatoVotacao dto = dgvcandidato.CurrentRow.DataBoundItem as VwConsultarCandidatoVotacao;


                imgcandidato.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);
               
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }




        }

        private void VotacaoPresidente_Load(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
             {
                VotoDTO dto = new VotoDTO();
                dto.id_candidato = 1;
                dto.id_eleitor = UserSession.UsuarioLogado.id_eleitor;

                VotoBusiness business = new VotoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Voto do presidente feito sucesso, agora vote no governador");

                VotaçãoGovernador tela = new VotaçãoGovernador();
                tela.Show();
                this.Hide();

            }
           catch(Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();

        }

        private void txtnumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {

        }

        private void dgvcandidato_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void imgcandidato_Click(object sender, EventArgs e)
        {

        }
    }
}
