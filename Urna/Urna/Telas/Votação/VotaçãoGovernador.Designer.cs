﻿namespace Urna.Telas
{
    partial class VotaçãoGovernador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.imgcandidato = new System.Windows.Forms.PictureBox();
            this.dgvcandidato = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnvoltar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgcandidato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcandidato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(204, 7);
            this.txtnumero.MaxLength = 2;
            this.txtnumero.Multiline = true;
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(389, 43);
            this.txtnumero.TabIndex = 31;
            this.txtnumero.TextChanged += new System.EventHandler(this.txtnumero_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Location = new System.Drawing.Point(599, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 44);
            this.button1.TabIndex = 30;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Transparent;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button13.Location = new System.Drawing.Point(108, 270);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(90, 44);
            this.button13.TabIndex = 29;
            this.button13.Text = "Confirmar";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Transparent;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button11.Location = new System.Drawing.Point(12, 270);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(90, 44);
            this.button11.TabIndex = 27;
            this.button11.Text = "Branco";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // imgcandidato
            // 
            this.imgcandidato.Image = global::Urna.Properties.Resources.logo;
            this.imgcandidato.Location = new System.Drawing.Point(22, 56);
            this.imgcandidato.Name = "imgcandidato";
            this.imgcandidato.Size = new System.Drawing.Size(176, 163);
            this.imgcandidato.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgcandidato.TabIndex = 26;
            this.imgcandidato.TabStop = false;
            // 
            // dgvcandidato
            // 
            this.dgvcandidato.AllowUserToAddRows = false;
            this.dgvcandidato.AllowUserToDeleteRows = false;
            this.dgvcandidato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvcandidato.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvcandidato.Location = new System.Drawing.Point(204, 56);
            this.dgvcandidato.Name = "dgvcandidato";
            this.dgvcandidato.ReadOnly = true;
            this.dgvcandidato.Size = new System.Drawing.Size(485, 258);
            this.dgvcandidato.TabIndex = 34;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "Nome";
            this.Column1.HeaderText = "Nome";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "Partido";
            this.Column2.HeaderText = "Partido";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "Cargo";
            this.Column3.HeaderText = "Cargo";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // btnvoltar
            // 
            this.btnvoltar.BackColor = System.Drawing.Color.Transparent;
            this.btnvoltar.BackgroundImage = global::Urna.Properties.Resources.unnamed;
            this.btnvoltar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnvoltar.Location = new System.Drawing.Point(22, 1);
            this.btnvoltar.Name = "btnvoltar";
            this.btnvoltar.Size = new System.Drawing.Size(64, 49);
            this.btnvoltar.TabIndex = 36;
            this.btnvoltar.TabStop = false;
            // 
            // VotaçãoGovernador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Urna.Properties.Resources._6da6a6a427abe92a953f4486cb78c7d0;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(701, 326);
            this.Controls.Add(this.btnvoltar);
            this.Controls.Add(this.dgvcandidato);
            this.Controls.Add(this.txtnumero);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.imgcandidato);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "VotaçãoGovernador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VotaçãoGovernador";
            ((System.ComponentModel.ISupportInitialize)(this.imgcandidato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcandidato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnvoltar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.PictureBox imgcandidato;
        private System.Windows.Forms.DataGridView dgvcandidato;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.PictureBox btnvoltar;
    }
}